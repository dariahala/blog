@extends('layouts.index')

@section('content')
    <div class="container">
        <div class="main-text">
            <h1 class="main-page-text">Блог по Google-Adsens</h1>
        </div>
        @if(isset($posts))
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-4" style="margin-top: 30px">
                        <a href="/post/{{ $post->slug }}">
                            <div class="card d-block ms-auto card-shadow">
                                <div class="card-header main-card-title">
                                    {{ $post->title }}
                                </div>
                                <img class="card-img-top main-image-card" src="{{ Voyager::image( $post->image ) }}"
                                     alt="Card image cap">
                                <div class="card-body main-description-card">
                                    <p class="card-text block-description-post">{{ $post->excerpt }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                <div class="mx-auto">
                    <br>
                    <div class="text-center">{{ $posts->links() }}</div>
                </div>
            </div>
        @endif
    </div>
@endsection