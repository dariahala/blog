<footer class="pt-4 mt-4 main-padding text-light dark-footer">
    <section class="container text-center text-md-left">
        <div class="row text-center text-md-left mt-3 pb-3">
            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">Company name</h6>
                <p class="text-muted">
                    Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit.
                </p>
            </div>
            <hr class="d-md-none">
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">Services</h6>
                <div class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </div>
            </div>
            <hr class="w-100 d-md-none">
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">Title text</h6>
                <div class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </div>
            </div>
            <hr class="w-100 d-md-none">
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">Title text</h6>
                <p class="text-muted">Vinnitsa</p>
                <p class="text-muted">blog@gmail.com</p>
                <p class="text-muted">+ 380 962 300 000</p>
                <p class="text-muted">+ 380 962 300 000</p>
            </div>
        </div>
        <hr>
        <div class="row py-3 d-flex">
            <div class="col-md-8 col-lg-8">
                <p class="text-center text-md-left grey-text">© 2018 Copyright: <strong class="text-muted"> Blog</strong></p>
            </div>
        </div>
    </section>
</footer>

