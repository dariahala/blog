@extends('layouts.index')

@section('content')

    <div class="container">
        @if(isset($posts))
            <div class="row">

                @foreach($posts as $post)
                    <div class="col-md-4" style="margin-top: 30px">
                        <a href="/post/{{ $post->slug }}">
                            <div class="card d-block ms-auto bg-card">
                                <div class="card-header">
                                    {{ $post->title }}
                                </div>
                                <img class="card-img-top" src="{{ Voyager::image( $post->image ) }}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text block-description-post">{{ $post->excerpt }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                <div class="mx-auto">
                    <br>
                    <div class="text-center">{{ $posts->links() }}</div>
                </div>
            </div>
        @endif
    </div>
@endsection