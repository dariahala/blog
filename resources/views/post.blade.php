@extends('layouts.index')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="post-title">{{ $post->title }}</h1>
            </div>
            <div class="col-md-4">
                <img class="post-img" src="{{ Voyager::image( $post->image ) }}" >
            </div>
            <div class="col-md-8 post-text">
                <p>{!! $post->body !!}</p>
            </div>
            <div class="col-md-12 post-time">
                <time datetime="2016-03-10 20:00">{{$post->created_at}}</time>
            </div>
        </div>
    </div>
    </div>
@endsection