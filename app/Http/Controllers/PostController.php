<?php

namespace App\Http\Controllers;

use App;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAllPost()
    {
        $posts = DB::table('posts')->paginate(9);
        return view('main/index', compact('posts'));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewPost($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        return view('post', compact('post'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function find(Request $request)
    {
        if ($request->title != '') {
            $posts = Post::where('title', "LIKE", '%' . $request->title . '%')->paginate(9);
        } else {
            $posts = Post::paginate(9);
        }

        return view('pages/find', [
            'posts' => $posts,
        ]);
    }
}
