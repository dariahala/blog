<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewPages($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();
        return view('pages/page', compact('page'));
    }
}
