<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * @return int
     */
    public function countPost(){
        return Post::all()->count();
    }
}
