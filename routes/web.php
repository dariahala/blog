<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@viewAllPost');

Route::get('post/{slug}', 'PostController@viewPost');

Route::get('page/{slug}', 'PageController@viewPages');

Route::get('posts/find', 'PostController@find');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
